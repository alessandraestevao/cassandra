package tech.mastertech.itau.cassproj.repositories;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.cassproj.models.Login;

public interface LoginRepository extends CrudRepository<Login, String>{

}
