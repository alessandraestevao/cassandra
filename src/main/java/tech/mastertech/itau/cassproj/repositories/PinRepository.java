package tech.mastertech.itau.cassproj.repositories;

import java.util.UUID;

import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.cassproj.models.PinUsuario;

public interface PinRepository extends CrudRepository<PinUsuario, String> {

	Iterable<PinUsuario> findAllByUsuario(String usuario);

//	@Query(value= "SELECT * FROM pinusuario WHERE usuario = ?1 and datapostagem =  ?2 ")
	PinUsuario findByUsuarioAndDataPostagem(String usuario, UUID dataPostagem);

}
