package tech.mastertech.itau.cassproj.repositories;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.cassproj.models.PinCategoria;

public interface PinCategoriaRepository extends CrudRepository<PinCategoria, String> {

	Iterable<PinCategoria> findAllByCategoria(String categoria);

}
