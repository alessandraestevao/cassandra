package tech.mastertech.itau.cassproj.dtos;

import java.util.UUID;

public class Pin {
	private String usuario;
	private String imagem;
	private String descricao;
	private String categoria;

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public UUID getDataPostagem() {
		return dataPostagem;
	}

	public void setDataPostagem(UUID dataPostagem) {
		this.dataPostagem = dataPostagem;
	}

	private UUID dataPostagem;

}
