package tech.mastertech.itau.cassproj.controller;

import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import tech.mastertech.itau.cassproj.dtos.Pin;
import tech.mastertech.itau.cassproj.models.PinCategoria;
import tech.mastertech.itau.cassproj.models.PinUsuario;
import tech.mastertech.itau.cassproj.services.PinService;

@RestController
@RequestMapping("/pin")
public class PinController {

	@Autowired
	private PinService pinService;

	private ModelMapper modelMapper = new ModelMapper();

	@PostMapping
	@ResponseStatus(value = HttpStatus.CREATED)
	public void cadastrarPin(@RequestBody PinUsuario pinUsuario) {

		pinService.cadastrarPin(pinUsuario);

	}

	@GetMapping
	public Object listarPins(@RequestParam(required = false) String categoria) {
		Iterable<PinUsuario> retornoUsuario;
		Iterable<PinCategoria> retornoCategoria;
		Iterable<PinUsuario> pinUsuario;
		Iterable<PinCategoria> pinCategoria;
		Object retorno;

		if (categoria == null) {
			retornoUsuario = pinService.listarTodos();
			retorno = retornoUsuario;
		} else {
			retornoCategoria = pinService.listarTodosPorCategoria(categoria);
			retorno = retornoCategoria;
		}

		return retorno;

	}

	@GetMapping("/{usuario}/{dataPostagem}")
	public Pin detalharPin(@PathVariable String usuario, @PathVariable UUID dataPostagem) {
		Pin retorno = pinService.detalharPin(usuario, dataPostagem);
		return retorno;
	}

	@GetMapping("/{usuario}/meusPins")
	public Iterable<PinUsuario> listarPinsDoUsuario(@PathVariable String usuario) {
		Iterable<PinUsuario> retorno = pinService.listarTodosPorUsername(usuario);
		return retorno;

	}

	@PostMapping("/{username}/favoritar")
	public void favoritarPin(@PathVariable String usuario) {
		pinService.favoritar(usuario);
	}

}
