package tech.mastertech.itau.cassproj.services;

import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.datastax.driver.core.utils.UUIDs;

import tech.mastertech.itau.cassproj.dtos.Pin;
import tech.mastertech.itau.cassproj.models.Perfil;
import tech.mastertech.itau.cassproj.models.PinCategoria;
import tech.mastertech.itau.cassproj.models.PinUsuario;
import tech.mastertech.itau.cassproj.repositories.PinCategoriaRepository;
import tech.mastertech.itau.cassproj.repositories.PinRepository;

@Service
public class PinService {

	@Autowired
	private PinRepository pinUsuarioRepository;

	@Autowired
	private PinCategoriaRepository pinCategoriaRepository;

	@Autowired
	private UsuarioService usuarioService;

	private ModelMapper modelMapper = new ModelMapper();

	public void cadastrarPin(PinUsuario pinUsuario) {
		pinUsuario.setDataPostagem(UUIDs.timeBased());
		PinUsuario pinUser = modelMapper.map(pinUsuario, PinUsuario.class);
		PinCategoria pinCategoria = modelMapper.map(pinUsuario, PinCategoria.class);
		pinUsuarioRepository.save(pinUser);
		pinCategoriaRepository.save(pinCategoria);

	}

	public Iterable<PinUsuario> listarTodos() {
		Iterable<PinUsuario> registros = pinUsuarioRepository.findAll();
		return registros;

	}

	public Iterable<PinUsuario> listarTodosPorUsername(String usuario) {

		Perfil perfilAcessado = verificaUsuario(usuario);
		Iterable<PinUsuario> registros = null;
		if (perfilAcessado != null) {
			return registros = pinUsuarioRepository.findAllByUsuario(usuario);

		}
		throw new ResponseStatusException(HttpStatus.NOT_FOUND);
	}

	private Perfil verificaUsuario(String usuario) {
		Perfil usuarioRegistrado = usuarioService.verificaUsuario(usuario);
		return usuarioRegistrado;

	}

	public void favoritar(String usuario) {

	}

	public Iterable<PinCategoria> listarTodosPorCategoria(String categoria) {
		Iterable<PinCategoria> registros = pinCategoriaRepository.findAllByCategoria(categoria);
		return registros;

	}

	public Pin detalharPin(String usuario, UUID dataPostagem) {
		PinUsuario pinUser = pinUsuarioRepository.findByUsuarioAndDataPostagem(usuario, dataPostagem);
		Pin pin =  modelMapper.map(pinUser, Pin.class);
		return pin;
	}

}
