package tech.mastertech.itau.cassproj.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import tech.mastertech.itau.cassproj.dtos.Usuario;
import tech.mastertech.itau.cassproj.models.Login;
import tech.mastertech.itau.cassproj.models.Perfil;
import tech.mastertech.itau.cassproj.repositories.LoginRepository;
import tech.mastertech.itau.cassproj.repositories.PerfilRepository;
import tech.mastertech.itau.cassproj.security.JwtTokenProvider;

@Service
public class UsuarioService {

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	private JwtTokenProvider tokenProvider;

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private PerfilRepository perfilRepository;

	public Optional<String> fazerLogin(Login credenciais) {
		Optional<Login> loginOptional = loginRepository.findById(credenciais.getUsuario());

		if (loginOptional.isPresent()) {
			if (passwordEncoder.matches(credenciais.getSenha(), loginOptional.get().getSenha())) {
				String token = tokenProvider.criarToken(credenciais.getUsuario());
				return Optional.of(token);
			}

		}
		return Optional.empty();
	}
	
	public Perfil verificaUsuario(String usuario) {
		Perfil perfilEncontrado = perfilRepository.findAllByUsuario(usuario);
		return perfilEncontrado;
	}

	public void cadastrar(Usuario usuario) {
		cadastrarLogin(usuario);
		cadastrarPerfil(usuario);

	}

	private void cadastrarPerfil(Usuario usuario) {

		Perfil perfil = new Perfil();
		perfil.setUsuario(usuario.getUsuario());
		perfil.setNome(usuario.getNome());
		perfil.setBio(usuario.getBio());
		perfil.setFotoPerfil(usuario.getFotoPerfil());

		perfilRepository.save(perfil);

	}

	private void cadastrarLogin(Usuario usuario) {

		Login login = new Login();
		String hash = passwordEncoder.encode(usuario.getSenha());

		login.setUsuario(usuario.getUsuario());
		login.setSenha(hash);

		loginRepository.save(login);
	}

}
